﻿namespace PBKDF2_Demo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saltLabel = new System.Windows.Forms.Label();
            this.saltLengthSlider = new System.Windows.Forms.TrackBar();
            this.saltInput = new System.Windows.Forms.TextBox();
            this.saltButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.iterationsLabel = new System.Windows.Forms.Label();
            this.iterationsSlider = new System.Windows.Forms.TrackBar();
            this.keyLengthSlider = new System.Windows.Forms.TrackBar();
            this.keyLengthLabel = new System.Windows.Forms.Label();
            this.passwordInput = new System.Windows.Forms.TextBox();
            this.hashButton = new System.Windows.Forms.Button();
            this.randomHashButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.hashOutput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // saltLabel
            // 
            this.saltLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.saltLabel.Location = new System.Drawing.Point(0, 0);
            this.saltLabel.Name = "saltLabel";
            this.saltLabel.Size = new System.Drawing.Size(240, 15);
            this.saltLabel.Text = "Salt Length: 32 Bytes";
            this.saltLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // saltLengthSlider
            // 
            this.saltLengthSlider.Dock = System.Windows.Forms.DockStyle.Top;
            this.saltLengthSlider.LargeChange = 8;
            this.saltLengthSlider.Location = new System.Drawing.Point(0, 15);
            this.saltLengthSlider.Maximum = 256;
            this.saltLengthSlider.Minimum = 8;
            this.saltLengthSlider.Name = "saltLengthSlider";
            this.saltLengthSlider.Size = new System.Drawing.Size(240, 20);
            this.saltLengthSlider.TabIndex = 1;
            this.saltLengthSlider.TickFrequency = 8;
            this.saltLengthSlider.Value = 32;
            this.saltLengthSlider.ValueChanged += new System.EventHandler(this.saltLengthSlider_ValueChanged);
            // 
            // saltInput
            // 
            this.saltInput.Dock = System.Windows.Forms.DockStyle.Top;
            this.saltInput.HideSelection = false;
            this.saltInput.Location = new System.Drawing.Point(0, 35);
            this.saltInput.MaxLength = 64;
            this.saltInput.Name = "saltInput";
            this.saltInput.Size = new System.Drawing.Size(240, 21);
            this.saltInput.TabIndex = 2;
            this.saltInput.WordWrap = false;
            this.saltInput.TextChanged += new System.EventHandler(this.saltInput_TextChanged);
            // 
            // saltButton
            // 
            this.saltButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.saltButton.Location = new System.Drawing.Point(0, 56);
            this.saltButton.Name = "saltButton";
            this.saltButton.Size = new System.Drawing.Size(240, 20);
            this.saltButton.TabIndex = 3;
            this.saltButton.Text = "Regenerate Salt";
            this.saltButton.Click += new System.EventHandler(this.saltButton_Click);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 76);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 25);
            // 
            // iterationsLabel
            // 
            this.iterationsLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.iterationsLabel.Location = new System.Drawing.Point(0, 101);
            this.iterationsLabel.Name = "iterationsLabel";
            this.iterationsLabel.Size = new System.Drawing.Size(240, 15);
            this.iterationsLabel.Text = "Iterations: 4096";
            this.iterationsLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // iterationsSlider
            // 
            this.iterationsSlider.Dock = System.Windows.Forms.DockStyle.Top;
            this.iterationsSlider.LargeChange = 1024;
            this.iterationsSlider.Location = new System.Drawing.Point(0, 116);
            this.iterationsSlider.Maximum = 65536;
            this.iterationsSlider.Minimum = 1024;
            this.iterationsSlider.Name = "iterationsSlider";
            this.iterationsSlider.Size = new System.Drawing.Size(240, 20);
            this.iterationsSlider.SmallChange = 256;
            this.iterationsSlider.TabIndex = 6;
            this.iterationsSlider.TickFrequency = 1024;
            this.iterationsSlider.Value = 4096;
            this.iterationsSlider.ValueChanged += new System.EventHandler(this.iterationsSlider_ValueChanged);
            // 
            // keyLengthSlider
            // 
            this.keyLengthSlider.Dock = System.Windows.Forms.DockStyle.Top;
            this.keyLengthSlider.LargeChange = 8;
            this.keyLengthSlider.Location = new System.Drawing.Point(0, 151);
            this.keyLengthSlider.Maximum = 256;
            this.keyLengthSlider.Minimum = 8;
            this.keyLengthSlider.Name = "keyLengthSlider";
            this.keyLengthSlider.Size = new System.Drawing.Size(240, 20);
            this.keyLengthSlider.TabIndex = 8;
            this.keyLengthSlider.TickFrequency = 8;
            this.keyLengthSlider.Value = 32;
            this.keyLengthSlider.ValueChanged += new System.EventHandler(this.keyLengthSlider_ValueChanged);
            // 
            // keyLengthLabel
            // 
            this.keyLengthLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.keyLengthLabel.Location = new System.Drawing.Point(0, 136);
            this.keyLengthLabel.Name = "keyLengthLabel";
            this.keyLengthLabel.Size = new System.Drawing.Size(240, 15);
            this.keyLengthLabel.Text = "Key Length: 32 Bytes";
            this.keyLengthLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // passwordInput
            // 
            this.passwordInput.Dock = System.Windows.Forms.DockStyle.Top;
            this.passwordInput.HideSelection = false;
            this.passwordInput.Location = new System.Drawing.Point(0, 171);
            this.passwordInput.MaxLength = 64;
            this.passwordInput.Name = "passwordInput";
            this.passwordInput.Size = new System.Drawing.Size(240, 21);
            this.passwordInput.TabIndex = 10;
            this.passwordInput.Text = "Testing";
            this.passwordInput.WordWrap = false;
            // 
            // hashButton
            // 
            this.hashButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.hashButton.Location = new System.Drawing.Point(0, 192);
            this.hashButton.Name = "hashButton";
            this.hashButton.Size = new System.Drawing.Size(240, 20);
            this.hashButton.TabIndex = 11;
            this.hashButton.Text = "Hash Password";
            this.hashButton.Click += new System.EventHandler(this.hashButton_Click);
            // 
            // randomHashButton
            // 
            this.randomHashButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.randomHashButton.Location = new System.Drawing.Point(0, 212);
            this.randomHashButton.Name = "randomHashButton";
            this.randomHashButton.Size = new System.Drawing.Size(240, 20);
            this.randomHashButton.TabIndex = 20;
            this.randomHashButton.Text = "Randomize and Hash Password";
            this.randomHashButton.Click += new System.EventHandler(this.randomHashButton_Click);
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 232);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(240, 25);
            // 
            // hashOutput
            // 
            this.hashOutput.Dock = System.Windows.Forms.DockStyle.Top;
            this.hashOutput.HideSelection = false;
            this.hashOutput.Location = new System.Drawing.Point(0, 257);
            this.hashOutput.MaxLength = 64;
            this.hashOutput.Name = "hashOutput";
            this.hashOutput.ReadOnly = true;
            this.hashOutput.Size = new System.Drawing.Size(240, 21);
            this.hashOutput.TabIndex = 14;
            this.hashOutput.WordWrap = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.hashOutput);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.randomHashButton);
            this.Controls.Add(this.hashButton);
            this.Controls.Add(this.passwordInput);
            this.Controls.Add(this.keyLengthSlider);
            this.Controls.Add(this.keyLengthLabel);
            this.Controls.Add(this.iterationsSlider);
            this.Controls.Add(this.iterationsLabel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.saltButton);
            this.Controls.Add(this.saltInput);
            this.Controls.Add(this.saltLengthSlider);
            this.Controls.Add(this.saltLabel);
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label saltLabel;
        private System.Windows.Forms.TrackBar saltLengthSlider;
        private System.Windows.Forms.TextBox saltInput;
        private System.Windows.Forms.Button saltButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label iterationsLabel;
        private System.Windows.Forms.TrackBar iterationsSlider;
        private System.Windows.Forms.TrackBar keyLengthSlider;
        private System.Windows.Forms.Label keyLengthLabel;
        private System.Windows.Forms.TextBox passwordInput;
        private System.Windows.Forms.Button hashButton;
        private System.Windows.Forms.Button randomHashButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox hashOutput;
    }
}


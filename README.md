# Project Description
Use **PBKDF2** in your Compact Framework project and remain compatible with desktop and server projects that use the **PBKDF2** functionality built into .NET framework.

Our company wanted to use a secure method of remote login credentials using Windows Mobile devices. The go to method for secure password storage and authentication is with **PBKDF2** (as described at this very informative password storage article), however the compact framework lacks the necessary classes (see the `Rfc2898DeriveBytes` Class). The compact framework is completely capable of the **PBKDF2** logic, so it was just a matter of porting the logic into the Compact Framework environment.  You may see that some others have created their own versions of **PBKDF2** (or attempted to at least), like OpenNETCF (with `PasswordDerivedBytes` and `HMACSHA1`) or BouncyCastle (though not specifically available, the components exist to product a **PBKDF2** function).

What I found after initially researching this topic was that OpenNETCF has a broken implementation and BouncyCastle was a lot of extra work just to get what I wanted (not to mention the massive footprint). What I really wanted something small, simple, and something that used as much built-in framework as possible.  The solution was reasonably simple, port the minimum required framework from the desktop assemblies into a small compact framework library.

I needed to port at the very least the `HMAC`, `HMACSHA1`, and `Rfc2898DeriveBytes`. In porting I decided to also include `DeriveBytes`, and `KeyedHashAlgorithm` (mostly for consistency and completeness).  I additionally included a small subset of the (internal) Utils class.  The port is almost verbatim, though a few changes were required, i'll detail the most obvious ones below.  The result is an almost identical **PBKDF2** library to the desktop assemblies which allows you to generate on desktop or CF and validate on desktop or CF, both functions will generate the same hashes.

### Minor Modifications
* `HashAlgorithm.HashValue` is protected internal, so I cannot access it directly from HMAC (I use the ever so slightly slower `.Hash` instead).
* `Buffer.InternalBlockCopy` is replaced with `Array.Copy` (probably slightly slower)
* All attribute decorations were removed
* All exceptions generate less than preferred exception messages (i don't have access to the internal resources to get the full exception message).

### WARNING
I cannot guarantee the safety of this library, it ***SHOULD*** be identical in operation as the desktop library but porting could have introduced one or more bugs. Please do your own due diligence and inspect the code for yourself before using it in any security critical way.

If anyone does discover an issue please [create an issue](https://bitbucket.org/Acrotech/pbkdf2cf/issues/new) so I can correct it. 

### Change Log
* 1.0
    * Initial release, CF access to PBKDF2 algorithm
* 1.1
    * Unified code base to target multiple frameworks
    * Added the PBKDF2Helper class (simplifies creating and validating packed hashes, as well as provides a simple method of packing randomized key length, salt length, and iterations when generating hashes)
    * Moved the namespaces around slightly and updated the assembly name.

### Roadmap
* CF20 project
* NET40 project
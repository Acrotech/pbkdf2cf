﻿namespace System.Security.Cryptography.PBKDF2.CF
{
    public static class Utils
    {
        public static byte[] Int(uint i)
        {
            byte[] bytes = BitConverter.GetBytes(i);
            byte[] buffer2 = new byte[] { bytes[3], bytes[2], bytes[1], bytes[0] };
            if (!BitConverter.IsLittleEndian)
            {
                return bytes;
            }
            return buffer2;
        }

        public static byte[] GenerateRandom(int keySize)
        {
            byte[] data = new byte[keySize];
            PBKDF2Helper.StaticRandomCryptoNumberGenerator.GetBytes(data);
            return data;
        }
    }
}
